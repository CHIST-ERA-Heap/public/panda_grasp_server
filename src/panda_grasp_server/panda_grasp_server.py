#!/usr/bin/env python
# -*- coding: utf-8 -*-
from shutil import ExecError
import sys
import math
import copy
import numpy as np
import rospy
import geometry_msgs
import moveit_commander
from moveit_commander.conversions import pose_to_list
import moveit_msgs.msg
import std_msgs.msg
from std_srvs.srv import Trigger, TriggerRequest, TriggerResponse
from tf.transformations import quaternion_from_matrix, quaternion_matrix, rotation_matrix
from threading import Lock
import grippers
import os
import tf
import rospkg
import graspa_utils

from panda_ros_common.msg import PandaState
from panda_ros_common.srv import (PandaGrasp, PandaGraspRequest, PandaGraspResponse,
                                    UserCmd,
                                    PandaMove, PandaMoveRequest, PandaMoveResponse,
                                    PandaMoveWaypoints, PandaMoveWaypointsRequest, PandaMoveWaypointsResponse,
                                    PandaHome, PandaHomeRequest, PandaHomeResponse,
                                    PandaSetHome, PandaSetHomeRequest, PandaSetHomeResponse,
                                    PandaGripperCommand, PandaGripperCommandRequest, PandaGripperCommandResponse,
                                    PandaGetState, PandaGetStateRequest, PandaGetStateResponse,
                                    PandaSetVelAccelScalingFactors, PandaSetVelAccelScalingFactorsRequest, PandaSetVelAccelScalingFactorsResponse,
                                    PandaAttachBox, PandaAttachBoxRequest, PandaAttachBoxResponse
                                    )

import actionlib
from franka_gripper.msg import GraspAction, GraspActionGoal, GraspActionResult
from franka_gripper.msg import MoveAction, MoveActionGoal, MoveActionResult
from franka_gripper.msg import StopAction, StopActionGoal, StopActionResult
from geometry_msgs.msg import Twist
from sensor_msgs.msg import Joy
from controller_manager_msgs.srv import ListControllers
from controller_manager_msgs.srv import SwitchController
from franka_msgs.msg import FrankaState
from digit_pubsub.srv import Save_image


def all_close(actual, goal, tolerance=0.01, verbose=True):
    """
    Convenience method for testing if a list of values are within a tolerance of their counterparts in another list
    @param: goal       A list of floats, a Pose or a PoseStamped
    @param: actual     A list of floats, a Pose or a PoseStamped
    @param: tolerance  A float
    @returns: bool
    """
    if verbose:
        print('actual: ', actual)
        print('goal: ', goal)

    all_equal = True
    if type(goal) is list:
        for index in range(len(goal)):
            value = abs(abs(actual[index]) - abs(goal[index]))
            # if(verbose and value > tolerance):
                # print("index: "+str(index))
                # print("actual[index]: "+str(actual[index]))
                # print("goal[index]: "+str(goal[index]))
                # print("goal difference: "+str(value))
            if (value > tolerance):
                if verbose:
                    print('all_close: ', False)
                return False
    elif type(goal) is geometry_msgs.msg.PoseStamped and type(actual) is geometry_msgs.msg.PoseStamped:
        return all_close(goal.pose, actual.pose, tolerance, verbose)
    elif type(goal) is geometry_msgs.msg.PoseStamped and type(actual)  is geometry_msgs.msg.Pose:
        return all_close(goal.pose, pose_to_list(actual), tolerance, verbose)
    elif type(goal) is geometry_msgs.msg.Pose and type(actual) is geometry_msgs.msg.Pose:
        return all_close(pose_to_list(goal), pose_to_list(actual), tolerance, verbose)
    elif type(goal) is geometry_msgs.msg.Pose and type(actual) is geometry_msgs.msg.PoseStamped:
        return all_close(pose_to_list(goal), actual.pose, tolerance, verbose)
    if verbose:
        print('all_close: ', True)

    return True

class NodeConfig(object):

    # Ugly but necessary dictionary container class
    # Translates string into class handle
    gripper_types = {
        "FRANKA_HAND"       : grippers.FrankaHandGripper,
        "ROBOTIQ_2F"        : grippers.Robotiq2FGripper,
        "ROBOTIQ_2F_FC"     : grippers.Robotiq2FGripperForceControlled
    }

    def __init__(self):

        # Configure service names
        self._user_cmd_service_name = rospy.get_param("~service_names/user_cmd_service", "~user_cmd")
        self._move_service_name = rospy.get_param("~service_names/panda_move_pose_service", "~panda_move_pose")
        self._grasp_service_name = rospy.get_param("~service_names/panda_grasp_service", "~panda_grasp")
        self._home_service_name = rospy.get_param("~service_names/panda_home_service", "~panda_home")
        self._move_wp_service_name = rospy.get_param("~service_names/panda_move_wp_service", "~panda_move_wp")
        self._set_home_service_name = rospy.get_param("~service_names/panda_set_home_pose_service", "~panda_set_home_pose")
        self._stop_service_name = rospy.get_param("~service_names/panda_stop_service", "~panda_stop")
        self._gripper_cmd_service_name = rospy.get_param("~service_names/panda_gripper_cmd_service", "~panda_gripper_cmd")
        self._get_robot_state_service_name = rospy.get_param("~service_names/panda_get_state_service", "~panda_get_state")
        self._set_scaling_factor_service_name = rospy.get_param("~service_names/panda_set_scaling_factors_service", "~panda_set_scaling_factors")
        self._recover_service_name = rospy.get_param("~service_names/panda_error_recover_service", "~panda_error_recover")
        self._attach_box_name = rospy.get_param("~service_names/panda_attach_box", "~panda_attach_box")

        # Configure robot homing poses
        home_position = rospy.get_param("~home_config/position", [0.5, 0.0, 0.5])
        home_orientation = rospy.get_param("~home_config/orientation", [1.0, 0.0, 0.0, 0.0]) # quaternion, xyzw format
        self._home_configs = {}
        self._home_configs['home_joints'] = rospy.get_param("~home_config/joints", [0.0, 0.0, 0.0, -1.570796327, 0.0, 1.570796327, 0.0])
        self._home_configs['home_pose'] = geometry_msgs.msg.Pose()
        self._home_configs['home_pose'].position.x = home_position[0]
        self._home_configs['home_pose'].position.y = home_position[1]
        self._home_configs['home_pose'].position.z = home_position[2]
        self._home_configs['home_pose'].orientation.x = home_orientation[0]
        self._home_configs['home_pose'].orientation.y = home_orientation[1]
        self._home_configs['home_pose'].orientation.z = home_orientation[2]
        self._home_configs['home_pose'].orientation.w = home_orientation[3]


        # Configure robot droping poses
        drop_position = rospy.get_param("~drop_config/position", [0.5, 0.0, 0.5])
        drop_orientation = rospy.get_param("~drop_config/orientation", [1.0, 0.0, 0.0, 0.0]) # quaternion, xyzw format
        self._drop_configs = {}
        #self._drop_configs['drop_joints'] = rospy.get_param("~drop_config/joints", [0.0, 0.0, 0.0, -1.570796327, 0.0, 1.570796327, 0.0])
        self._drop_configs['drop_pose'] = geometry_msgs.msg.Pose()
        self._drop_configs['drop_pose'].position.x = drop_position[0]
        self._drop_configs['drop_pose'].position.y = drop_position[1]
        self._drop_configs['drop_pose'].position.z = drop_position[2]
        self._drop_configs['drop_pose'].orientation.x = drop_orientation[0]
        self._drop_configs['drop_pose'].orientation.y = drop_orientation[1]
        self._drop_configs['drop_pose'].orientation.z = drop_orientation[2]
        self._drop_configs['drop_pose'].orientation.w = drop_orientation[3]


        # Configure scene parameters
        self._table_height = rospy.get_param("~workspace/table_height", 0.13) # z distance from upper side of the table block, from the robot base ref frame
        self._table_size = rospy.get_param("~workspace/table_size", (1.0, 2.0, 0.8)) # x y z size of table block
        self._robot_workspace = None
        self._bench_dimensions = rospy.get_param("~workspace/bench_size", (0.6, 0.6, 0.6)) # x y z
        self._bench_mount_point_xy = rospy.get_param("~workspace/bench_mount_xy", (0.1, 0.0)) # x y wrt center of the bench

        self._obstacle1_height = rospy.get_param("~workspace/obstacle1_height", 0.13) # z distance from upper side of the table block, from the robot base ref frame
        self._obstacle1_size = rospy.get_param("~workspace/obstacle1_size", (1.0, 2.0, 0.8)) # x y z size of table block
        self._obstacle1_position = rospy.get_param("~workspace/obstacle1_position", [0.5, 0.0, 0.5])

        self._obstacle2_height = rospy.get_param("~workspace/obstacle2_height", 0.13) # z distance from upper side of the table block, from the robot base ref frame
        self._obstacle2_size = rospy.get_param("~workspace/obstacle2_size", (1.0, 2.0, 0.8)) # x y z size of table block
        self._obstacle2_position = rospy.get_param("~workspace/obstacle2_position", [0.5, 0.0, 0.5])

        self._obstacle3_height = rospy.get_param("~workspace/obstacle3_height", 0.13) # z distance from upper side of the table block, from the robot base ref frame
        self._obstacle3_size = rospy.get_param("~workspace/obstacle3_size", (1.0, 2.0, 0.8)) # x y z size of table block
        self._obstacle3_position = rospy.get_param("~workspace/obstacle3_position", [0.5, 0.0, 0.5])

        self._obstacle4_height = rospy.get_param("~workspace/obstacle4_height", 0.13) # z distance from upper side of the table block, from the robot base ref frame
        self._obstacle4_size = rospy.get_param("~workspace/obstacle4_size", (1.0, 2.0, 0.8)) # x y z size of table block
        self._obstacle4_position = rospy.get_param("~workspace/obstacle4_position", [0.5, 0.0, 0.5])

        # Configure speed/accel scaling factors
        self._max_velocity_scaling_factor = rospy.get_param("~planning/max_vel_scaling_factor", 0.3)
        self._max_acceleration_scaling_factor = rospy.get_param("~planning/max_acc_scaling_factor", 0.3)

        # Configure planner ID
        self._planner_id = rospy.get_param("~planning/planner_id", "RRTkConfigDefault")

        # Configure the end effector to use in the move group
        self._eef_link_id = rospy.get_param("~planning/eef_link_id", "panda_tcp")

        # Enable Rviz visualization of trajectories
        self._publish_rviz = rospy.get_param("~planning/publish_rviz", True)

        # Configure gripper
        # Choose gripper type
        self._gripper_type = rospy.get_param("~gripper/gripper_type", "FRANKA_HAND")
        self._min_width = rospy.get_param("~gripper/min_width", 0.0)#= 0.0                        # m
        self._max_width = rospy.get_param("~gripper/max_width", 0.09)#= 0.09
        self._min_speed = rospy.get_param("~gripper/min_speed", 0.01)#= 0.01                       # m/s
        self._max_speed = rospy.get_param("~gripper/max_speed", 0.1)#= 0.1
        self._min_force = rospy.get_param("~gripper/min_force", 10)#= 10                         # N
        self._max_force = rospy.get_param("~gripper/max_force", 70)#= 70

        # Enable force-controlled grasping
        self._enable_force_grasp = rospy.get_param("~ops_params/enable_force_grasp", False)

        # Enable GRASPA stability motion after grasp
        self._enable_graspa_stab_motion = rospy.get_param("~ops_params/enable_graspa_stab_motion", False)

        #stop before closing the gripper
        self._stop_before_closing = rospy.get_param("~ops_params/stop_before_closing", False)

        # Save grasp path
        self._save_grasp = rospy.get_param("~ops_params/enable_graspa_save_grasp", False)
        rospack = rospkg.RosPack()
        self._grasp_save_path = rospy.get_param("~ops_params/grasp_save_path", os.path.join(rospack.get_path('panda_grasp_server'), 'dumped_grasps'))

class PandaActionServer(object):

    def __init__(self, config):

        # Configure moveit
        moveit_commander.roscpp_initialize(sys.argv)
        self._robot = moveit_commander.RobotCommander()

        self._group_name = "panda_arm"
        self._move_group = moveit_commander.MoveGroupCommander(
            self._group_name)
        self._move_group.set_end_effector_link(config._eef_link_id)
        self._eef_link = self._move_group.get_end_effector_link()

        self._max_velocity_scaling_factor = config._max_velocity_scaling_factor
        self._max_acceleration_scaling_factor = config._max_acceleration_scaling_factor

        self._move_group.set_max_velocity_scaling_factor(config._max_velocity_scaling_factor)
        self._move_group.set_max_acceleration_scaling_factor(config._max_acceleration_scaling_factor)

        self._move_group.set_planner_id(config._planner_id)


        group_hand = "hand"
        self._move_group_hand = moveit_commander.MoveGroupCommander(group_hand)
        # Display trajectories in Rviz
        self._publish_rviz = config._publish_rviz
        self._display_trajectory_publisher = None
        if self._publish_rviz:
            self._display_trajectory_publisher = rospy.Publisher('/move_group/display_planned_path',
                                                                 moveit_msgs.msg.DisplayTrajectory,
                                                                 queue_size=10)

        self._scene = moveit_commander.PlanningSceneInterface()
        self._scene.remove_world_object()

        # Configure status monitor condition
        self._stop_mutex = Lock()
        self._motion_stopped = False

        # Set the force grasp flag
        self._enable_force_grasp = config._enable_force_grasp

        # Set the stability motion enable flag
        self._enable_graspa_stab_motion = config._enable_graspa_stab_motion

        # Set the only lift enable flag
        self._stop_before_closing = config._stop_before_closing

        # Set path to save grasps to
        self._save_grasp = config._save_grasp
        self._grasp_save_path = config._grasp_save_path

        # Configure grasping server
        self._grasp_service = rospy.Service(config._grasp_service_name,
                                            PandaGrasp,
                                            self.do_grasp_callback)

        # Configure movement server
        self._movement_server = rospy.Service(config._move_service_name,
                                              PandaMove,
                                              self.go_to_pose_callback)

        # Configure homing server
        self._homing_server = rospy.Service(config._home_service_name,
                                            PandaHome,
                                            self.go_home_callback)
        
        print('joints: ', self._move_group.get_current_joint_values())
        # Configure homing pose setting server
        self._set_home_pose_server = rospy.Service(config._set_home_service_name,
                                                    PandaSetHome,
                                                    self.set_home_pose_callback)

        # Configure trajectory movement server
        self._wp_movement_server = rospy.Service(config._move_wp_service_name,
                                                PandaMoveWaypoints,
                                                self.execute_trajectory_callback)

        # Configure motion stop server
        self._movement_stop_server = rospy.Service(config._stop_service_name,
                                                    Trigger,
                                                    self.stop_motion_callback)

        # Configure error clear server
        self._error_recover_server = rospy.Service(config._recover_service_name,
                                                   Trigger,
                                                   self.recover_error_callback)

        # Configure gripper closing server
        self._gripper_cmd_server = rospy.Service(config._gripper_cmd_service_name,
                                                    PandaGripperCommand,
                                                    self.gripper_command_callback)

        # Configure get status server
        self._get_robot_state_server = rospy.Service(config._get_robot_state_service_name,
                                                    PandaGetState,
                                                    self.get_state_callback)

        # Configure speed changing server
        self._set_vel_accel_server = rospy.Service(config._set_scaling_factor_service_name,
                                                PandaSetVelAccelScalingFactors,
                                                self.set_vel_accel_scaling_factor_callback)

        # Configure gripper action clients
        '''
        self._min_width = rospy.get_param("~gripper/min_width", 0.0)#= 0.0                        # m
        self._max_width = rospy.get_param("~gripper/max_width", 0.09)#= 0.09
        self._min_speed = rospy.get_param("~gripper/min_speed", 0.01)#= 0.01                       # m/s
        self._max_speed = rospy.get_param("~gripper/max_speed", 0.1)#= 0.1
        self._min_force = rospy.get_param("~gripper/min_force", 10)#= 10                         # N
        self._max_force = rospy.get_param("~gripper/max_force", 70)#= 70
        '''
        self._gripper = config.gripper_types[config._gripper_type](
            gripper_name=config._gripper_type,
            min_width=config._min_width,
            max_width=config._max_width,
            min_speed=config._min_speed,
            max_speed=config._max_speed,
            min_force=config._min_force,
            max_force=config._max_force
            )
        rospy.loginfo("Gripper setup complete")

        # Configure TF transform listener
        self._tf_listener = tf.TransformListener(True, rospy.Duration(10))

        # Configure home pose
        self._home_pose = config._home_configs['home_pose']

        # Alternative home pose in joint values
        # This home pose is the same defined in the moveit package
        self._home_pose_joints = self._move_group.get_current_joint_values()
        self._home_pose_joints[0:7] = config._home_configs['home_joints']

        # Configure home pose
        self._drop_pose = config._drop_configs['drop_pose']

        # Alternative drop pose in joint values
        #self._drop_pose_joints = self._move_group.get_current_joint_values()
        #self._drop_pose_joints[0:7] = config._drop_configs['drop_joints']

        # Add table as a collision object
        if config._table_height is not None:
            rospy.sleep(1)
            table_pose = geometry_msgs.msg.PoseStamped()
            table_pose.header.frame_id = self._robot.get_planning_frame()
            table_pose.pose.position.x = 0.2 + config._table_size[0]/2
            table_pose.pose.position.y = 0.0
            table_pose.pose.position.z = config._table_height - config._table_size[2]/2
            self._scene.add_box("table", table_pose, config._table_size)

        if config._obstacle1_height is not None:
            rospy.sleep(1)
            obstacle1_pose = geometry_msgs.msg.PoseStamped()
            obstacle1_pose.header.frame_id = self._robot.get_planning_frame()
            obstacle1_pose.pose.position.x = config._obstacle1_position[0] + config._obstacle1_size[0]/2
            obstacle1_pose.pose.position.y = config._obstacle1_position[1]
            obstacle1_pose.pose.position.z = config._obstacle1_position[2] + config._obstacle1_height - config._obstacle1_size[2]/2
            self._scene.add_box("obstacle1", obstacle1_pose, config._obstacle1_size)

        if config._obstacle2_height is not None:
            rospy.sleep(1)
            obstacle2_pose = geometry_msgs.msg.PoseStamped()
            obstacle2_pose.header.frame_id = self._robot.get_planning_frame()
            obstacle2_pose.pose.position.x = config._obstacle2_position[0] + config._obstacle2_size[0]/2
            obstacle2_pose.pose.position.y = config._obstacle2_position[1]
            obstacle2_pose.pose.position.z = config._obstacle2_position[2] + config._obstacle2_height - config._obstacle2_size[2]/2
            self._scene.add_box("obstacle2", obstacle2_pose, config._obstacle2_size)

        if config._obstacle3_height is not None:
            rospy.sleep(1)
            obstacle3_pose = geometry_msgs.msg.PoseStamped()
            obstacle3_pose.header.frame_id = self._robot.get_planning_frame()
            obstacle3_pose.pose.position.x = config._obstacle3_position[0] + config._obstacle3_size[0]/2
            obstacle3_pose.pose.position.y = config._obstacle3_position[1]
            obstacle3_pose.pose.position.z = config._obstacle3_position[2] + config._obstacle3_height - config._obstacle3_size[2]/2
            self._scene.add_box("obstacle3", obstacle3_pose, config._obstacle3_size)

        if config._obstacle4_height is not None:
            rospy.sleep(1)
            obstacle4_pose = geometry_msgs.msg.PoseStamped()
            obstacle4_pose.header.frame_id = self._robot.get_planning_frame()
            obstacle4_pose.pose.position.x = config._obstacle4_position[0] + config._obstacle4_size[0]/2
            obstacle4_pose.pose.position.y = config._obstacle4_position[1]
            obstacle4_pose.pose.position.z = config._obstacle4_position[2] + config._obstacle4_height - config._obstacle4_size[2]/2
            self._scene.add_box("obstacle4", obstacle4_pose, config._obstacle4_size)

        # Add the workbench
        rospy.sleep(1)
        workbench_pose = geometry_msgs.msg.PoseStamped()
        workbench_pose.header.frame_id = self._robot.get_planning_frame()
        workbench_pose.pose.position.x = -config._bench_mount_point_xy[0]
        workbench_pose.pose.position.y = config._bench_mount_point_xy[1]
        workbench_pose.pose.position.z = -config._bench_dimensions[2]/2

        # Turn off collisions between panda_link0 and workbench
        # self._scene.add_box("workbench", workbench_pose, config._bench_dimensions)
        self._scene.attach_box('panda_link0', 'workbench', pose=workbench_pose, size=config._bench_dimensions, touch_links=['panda_link0', 'panda_link1'])

        # Not sure why editing the ACM does not work, I'll leave code here anyway
        # from moveit_msgs.msg import PlanningScene, PlanningSceneComponents
        # from moveit_msgs.srv import GetPlanningScene
        # pub_planning_scene = rospy.Publisher('/planning_scene', PlanningScene, queue_size=10)
        # rospy.wait_for_service('/get_planning_scene', 10.0)
        # get_planning_scene = rospy.ServiceProxy('/get_planning_scene', GetPlanningScene)
        # request = PlanningSceneComponents(components=PlanningSceneComponents.ALLOWED_COLLISION_MATRIX)
        # response = get_planning_scene(request)
        # acm = response.scene.allowed_collision_matrix
        # if not 'workbench' in acm.default_entry_names:
        #     # add button to allowed collision matrix
        #     acm.default_entry_names += ['workbench']
        #     acm.default_entry_values += [True]

        #     planning_scene_diff = PlanningScene(
        #             is_diff=True,
        #             allowed_collision_matrix=acm)

        #     pub_planning_scene.publish(planning_scene_diff)
        #     rospy.sleep(1.0)
        
        self.cmd_vel_pub = rospy.Publisher("/franka_control/target_velocity", Twist, queue_size=1)
        self.target_vel_sub = rospy.Subscriber("/franka_control/current_target_velocity", Twist,
                                               self.target_vel_callback)

        self.state_subscriber = rospy.Subscriber('/franka_state_controller/franka_states',
                                                 FrankaState,
                                                 self.update_state)

        self.O_P_EE = None   # current position of EE in link 0 frame
        self.O_V_EE = None   # current cartesian velocity of EE in link 0 frame
        self.O_P_EE_prev = None   # previous position of EE in link 0 frame
        self.O_V_EE_prev = None   # previous cartesian velocity of EE in link 0 frame

        self.O_P_EE_timestamp_secs = None
        self.O_P_EE_timestamp_secs_prev = None
        self.vel_scale = 1.0
        
        # Safety zone constraint based on joint positions published on
        # /franka_state_controller/franka_states. Order is x,y,z.
        # Up/Down [0.005, 0.32]
        # Forward/Backward [0.33, 0.6]
        # Left/Right [-0.4, 0.35]
        #self.safety_region = np.array([[0.33, 0.7], [-0.3, 0.05], [0.05, 0.41]])
        self.ee_safety_zone=[[-np.inf, np.inf], [-np.inf, np.inf], [-np.inf, np.inf]]

        self.time_since_last_collision = rospy.Time.now()        
        self.joy_sub = rospy.Subscriber("/joy", Joy, self.joy_callback)
        self.memory = []
        self.init_save_image_srv()

        # Configure grasping server
        self._attach_box_service = rospy.Service(config._attach_box_name,
                                            PandaAttachBox,
                                            self.attach_box_callback)
        

    def save_image_srv(self): 
        pass
    
    def init_save_image_srv(self):
        try:
            rospy.loginfo("Minimum: Waiting for digit service...")
            rospy.wait_for_service('save_image_srv', timeout=3)
            self.save_image_srv = rospy.ServiceProxy('save_image_srv', Save_image)
            rospy.loginfo("...Connected with service {}".format('save_image_srv'))
        # except rospy.ServiceException as e:
            self.wait_time_save_image_srv = 2.0
        except Exception as e:
            print("Service call failed: ", e)
            self.wait_time_save_image_srv = 0.0

    def shift_ee_by(self, axis=0, value=0.0, wait_to_finish=True):

        # dP = np.array([0.0, 0.0, 0.0])
        # dP[axis] = value
        # req = self.O_P_EE + dP
        # if not self.ee_inside_safety_zone(req):
        #     raise Exception('Shifted ee pose should be inside the safety zone {}'.format(req,
        #                                                                                  self.ee_safety_zone))
        
        "axis in 0...5 for (x,y,z, r, p, y), value denotes the relative delta"
        self._move_group.shift_pose_target(axis, value)

        # Now, we call the planner to compute the plan and execute it.
        plan = self._move_group.go(wait=wait_to_finish)
        # Calling `stop()` ensures that there is no residual movement
        self._move_group.stop()
        # It is always good to clear your targets after planning with poses.
        # Note: there is no equivalent function for clear_joint_value_targets()
        self._move_group.clear_pose_targets()

        # if not running:
        #     print ("Switching to velocity control")
        #     self.switch_controllers('position_joint_trajectory_controller',
        #                             'cartesian_velocity_example_controller')

        return True

    def update_state(self, msg):

        self.O_P_EE_prev = self.O_P_EE
        self.O_V_EE_prev = self.O_V_EE
        self.O_P_EE_timestamp_secs_prev = self.O_P_EE_timestamp_secs

        self.O_P_EE = np.array(msg.O_T_EE[-4:-1])
        self.O_P_EE_timestamp_secs = msg.header.stamp.secs + msg.header.stamp.nsecs*(1e-9)

        # if self.O_P_EE_prev is None and not self.ee_inside_safety_zone(self.O_P_EE):
        #     raise Exception('Initial position {} should be inside the safety zone {}'.format(self.O_P_EE,
        #                                                                                      self.ee_safety_zone))
        
        if (self.O_P_EE_prev is not None):
            dt = self.O_P_EE_timestamp_secs - self.O_P_EE_timestamp_secs_prev
            assert (dt > 0)
            self.O_V_EE = (self.O_P_EE - self.O_P_EE_prev) / dt

        # Filter velocity (not sure if this is necessary)
        if (self.O_V_EE_prev is not None):
            self.O_V_EE = 0.5 * self.O_V_EE + 0.5*self.O_V_EE_prev

        self.franka_state = msg
        #print (self.get_ee_state())

    def target_vel_callback(self, twist):
        self.current_target_velocity = twist

    def set_ee_velocity(self, twist):

        if self.O_P_EE is None:
            return
        
        # #
        # # Clip x-velocity if outsize the safety zone and request is to further violate it
        # #
        # if self.O_P_EE[0] < self.ee_safety_zone[0][0] and twist.linear.x < 0:
        #     twist.linear.x = 0.0
        
        # if self.O_P_EE[0] > self.ee_safety_zone[0][1] and twist.linear.x > 0:
        #     twist.linear.x = 0.0

        # #
        # # Clip y-velocity if outsize the safety zone and request is to further violate it
        # #
        # if self.O_P_EE[1] < self.ee_safety_zone[1][0] and twist.linear.y < 0:
        #     twist.linear.y = 0.0
        
        # if self.O_P_EE[1] > self.ee_safety_zone[1][1] and twist.linear.y > 0:
        #     twist.linear.y = 0.0


        # #
        # # Clip z-velocity if outsize the safety zone and request is to further violate it
        # #
        # if self.O_P_EE[2] < self.ee_safety_zone[2][0] and twist.linear.z < 0:
        #     twist.linear.z = 0.0
        
        # if self.O_P_EE[2] > self.ee_safety_zone[2][1] and twist.linear.z > 0:
        #     twist.linear.z = 0.0
        
        # print('self.cmd_vel_pub.publish(twist): ', twist)
        self.cmd_vel_pub.publish(twist)
    
    def change_controller(self):
        current_pose = self._move_group.get_current_pose().pose
        print("current_pose: ", current_pose)

        running = self.controller_is_running(
            'my_cartesian_velocity_controller')
        if not running:
            print("Switching to velocity control")
            self.switch_controllers('position_joint_trajectory_controller',
                                    'my_cartesian_velocity_controller')
        else:
            print("Switching to position_joint_trajectory_controller")
            self.switch_controllers('my_cartesian_velocity_controller',
                                    'position_joint_trajectory_controller')

    def switch_controllers(self, stop_controller, start_controller):
        rospy.wait_for_service('/controller_manager/switch_controller')
        try:
            srv = rospy.ServiceProxy('/controller_manager/switch_controller', SwitchController)
            req = {'start_controllers': [start_controller],
                   'stop_controllers': [stop_controller],
                   'strictness': 2,  # 2=strict, 1=best effort
                   # 'start_asap': True, # for some reason this was excluded from ROS melodic
                   # 'timeout': 3,  # in seconds
                   }

            resp = srv(**req)
            return resp
        except Exception as e:
            print("Service call failed: %s" % e)

    def controller_is_running(self, name):
        controller_status = self.query_controller_status()
        for cs in controller_status.controller:
            if cs.name == name:
                return cs.state == 'running'

        return False

    def query_controller_status(self):
        rospy.wait_for_service('/controller_manager/list_controllers')
        try:
            srv = rospy.ServiceProxy('/controller_manager/list_controllers', ListControllers)
            resp = srv()
            return resp
        except Exception as e:
            print("Service call failed: %s" % e)

    def joy_callback(self, data):
        data_axes = list(data.axes)
        data_buttons = list(data.buttons)
        for i in range(len(data_axes)):
            if(abs(data_axes[i]) < 0.3):
                data_axes[i]=0.0

        # print('data_axes: ',data_axes)
        # print('data_buttons: ',data_buttons)

        if self.memory != data_buttons:
            if(data_buttons[6]): #select
                self.go_home(use_joints=True, home_gripper=False, go_drop=False, speed=None)
                print('joints: ', self.get_joints_state())
                print('pose: ', self._move_group.get_current_pose().pose)

            if(data_buttons[7]): #start
                self.change_controller()
            if(data_buttons[2]): #X
                self.close_gripper(speed=None, target_force=None)

            if (data_buttons[1]): #B
                 self.open_gripper(speed=None)
      
            #todo check if gripper is closed
            if (data_buttons[3]): #Y
                running = self.controller_is_running('position_joint_trajectory_controller')
                if not running:
                    self.change_controller()
                print('test grasp')
                self.test_grasp(release=False)
                #self.attempt_grasp_refinement()

            if (data_buttons[0]): #A
                try:
                    self.save_image_srv()
                except Exception as e:
                    print(e)

            if (data_buttons[5]): #RB
                gripper_width = self._move_group_hand.get_current_joint_values()[0] + self._move_group_hand.get_current_joint_values()[1]
                gripper_width += 0.01
                self.command_gripper(gripper_width)

            if (data_buttons[4]): #B
                gripper_width = self._move_group_hand.get_current_joint_values()[0] + self._move_group_hand.get_current_joint_values()[1]
                gripper_width -= 0.01
                self.command_gripper(gripper_width)

            #     sucess, current_pose = self.panda_client.go_to_pose_goal(x=0.5,
            #                                                                 y=0.45,
            #                                                                 z=0.45,
            #                                                                 roll=-math.pi,
            #                                                                 pitch=-0.6,
            #                                                                 yaw=-math.pi/2,
            #                                                                 tolerance=0.5)
            # if (data_buttons[0]): #A

            #     sucess, current_pose = self.panda_client.go_to_pose_goal(x=0.5,
            #                                                                 y=-0.45,
            #                                                                 z=0.45,
            #                                                                 roll=-math.pi,
            #                                                                 pitch=-0.6,
            #                                                                 yaw=math.pi/2,
            #                                                             tolerance=0.5)

        self.memory = copy.deepcopy(data_buttons)

        # if data_buttons[4]!=0 or data_axes[6]!=0 or data_axes[7]!=0:
        #     running = self.controller_is_running('position_joint_trajectory_controller')
        #     if not running:
        #         self.change_controller()
        #     if data_buttons[4]:
        #         self.shift_ee_by(axis=3, value=self.vel_scale  * data_buttons[4]/30.0)
        
        #     if data_axes[6]:
        #         self.shift_ee_by(axis=4, value=self.vel_scale  * data_axes[6]/30.0)
        
        #     if data_axes[7]:
        #         self.shift_ee_by(axis=5, value=self.vel_scale  * data_axes[7]/30.0)  # YAW

        #     self.change_controller()


        # Publish Twist
        twist = Twist()

        # X - Forward/Backward
        # Y - Left/Right
        # Z - Up/Down

        twist.linear.x = self.vel_scale * data_axes[0] * -1
        twist.linear.y = self.vel_scale * data_axes[1]
        twist.linear.z = self.vel_scale * data_axes[7]

        twist.angular.x = self.vel_scale * data_axes[4] * 4 * -1
        twist.angular.y = self.vel_scale * data_axes[3] * 4 * -1
        twist.angular.z = self.vel_scale * data_axes[6] * 4 * -1

        self.set_ee_velocity(twist)
        #print('pose: ', self._move_group.get_current_pose().pose)

    def set_stopped_status(self, stopped=True):

        # Set the motion stopped status of the server
        # Stopped = True means robot has been stopped
        # Necessary to stop trajectory execution

        self._stop_mutex.acquire()
        self._motion_stopped = stopped
        self._stop_mutex.release()

    def set_home(self, pos, quat):

        self._home_pose.orientation.x = quat[0]
        self._home_pose.orientation.y = quat[1]
        self._home_pose.orientation.z = quat[2]
        self._home_pose.orientation.w = quat[3]

        self._home_pose.position.x = pos[0]
        self._home_pose.position.y = pos[1]
        self._home_pose.position.z = pos[2]

    def set_home_pose_callback(self, req):

        # Callback for setting home state of the robot

        use_joint_values = req.use_joints

        ok_msg = (req.home_joints is not None) or (req.home_pose.pose is not None)

        if ok_msg:
            if use_joint_values:
                self._home_pose_joints[0:7] = req.home_joints
            else:
                self._home_pose = req.home_pose.pose
            rospy.loginfo("New homing pose set")
            return True
        else:
            return False

    def attach_box_callback(self, req):
        print('attach_box_callback: ', req)
        #callback to attach box
        if req.remove:
            try:
                # Let go of the collision box
                self._scene.remove_attached_object(req.link, req.name)
                while len(self._scene.get_attached_objects([req.name]).keys()):
                    rospy.sleep(0.1)
                self._scene.remove_world_object(req.name)
            except Exception as e:
                print(e)
            
        else:
            # Add a collision volume to the end effector, to make sure
            # whatever object is being carried does not bump into the table or anything else
            attached_object_pose = geometry_msgs.msg.PoseStamped()
            attached_object_pose.header.frame_id = self._eef_link
            attached_object_pose.pose.orientation = req.pose.pose.orientation
            attached_object_pose.pose.position = req.pose.pose.position

            self._scene.attach_box(req.link, req.name, pose=attached_object_pose, size=req.size,
                                    touch_links=req.touch_links)
        return True


    def go_home(self, use_joints=False, home_gripper=True, go_drop=False, speed=None):
        running = self.controller_is_running('position_joint_trajectory_controller')
        if not running:
            self.change_controller()

        if home_gripper:
            self.open_gripper(speed)

        if go_drop:
            drop_pose = geometry_msgs.msg.Pose()

            drop_pose.orientation.x = self._drop_pose.orientation.x
            drop_pose.orientation.y = self._drop_pose.orientation.y
            drop_pose.orientation.z = self._drop_pose.orientation.z
            drop_pose.orientation.w = self._drop_pose.orientation.w

            drop_pose.position.x = self._drop_pose.position.x
            drop_pose.position.y = self._drop_pose.position.y
            drop_pose.position.z = self._drop_pose.position.z

            if not self.go_to_pose(drop_pose, "Moving to drop_pose pose"):
                return False
        else:

            # Move the robot in home pose
            # use_joints flag uses the joint config instead of pose
            if use_joints:
                self._move_group.set_joint_value_target(self._home_pose_joints)
            else:
                self._move_group.set_pose_target(self._home_pose)
            self._move_group.go(wait=True)
            self._move_group.stop()
            self._move_group.clear_pose_targets()
            if not all_close(self._move_group.get_current_pose().pose, self._home_pose):
                return False

        return True

    def close_gripper(self, speed=None, target_force=None, wait=True, timeout=None):

        return self._gripper.close_gripper(target_speed=speed, target_force= target_force, wait=wait, timeout=timeout)

    def open_gripper(self, speed=None, wait=True, timeout=None):

        return self._gripper.open_gripper(target_speed=speed, wait=wait, timeout=timeout)

    def command_gripper(self, gripper_width, target_force=None, velocity=None, wait=True, timeout=None):#velocity=0.5

        # This command just moves the fingers. Actual gripper behaviour
        # depends on what type of gripper is actually used

        return self._gripper.move_fingers(target_width=gripper_width, target_speed=velocity, target_force=target_force, wait=wait, timeout=timeout)

    def get_gripper_state(self):
        # TODO reimplement this according to new grippers module
        joint_poses = self._move_group_hand.get_current_joint_values()
        #joint_poses = [0.0]
        return joint_poses

    def get_joints_state(self):
        joint_poses = self._move_group.get_current_joint_values()
        return joint_poses

    def get_current_pose_EE(self):

        quaternion = [self._move_group.get_current_pose().pose.orientation.x,
                      self._move_group.get_current_pose().pose.orientation.y,
                      self._move_group.get_current_pose().pose.orientation.z,
                      self._move_group.get_current_pose().pose.orientation.w]

        position = [self._move_group.get_current_pose().pose.position.x,
                    self._move_group.get_current_pose().pose.position.y,
                    self._move_group.get_current_pose().pose.position.z]

        return [position, quaternion]

    def get_state_callback(self, req):

        # Return the state of the robot joints, gripper width and eef pose
        robot_state = PandaState()

        # EEF pose
        robot_state.eef_state = self._move_group.get_current_pose()

        # Joints state
        robot_state.joints_state = self._move_group.get_current_joint_values()

        # Gripper width
        # TODO: reimplement this!
        width = self._move_group_hand.get_current_joint_values()[0] + self._move_group_hand.get_current_joint_values()[1]
        robot_state.gripper_state = width
        #robot_state.gripper_state = 0.0

        return PandaGetStateResponse(robot_state=robot_state)

    def go_to_pose(self, target_pose, message=None):
        running = self.controller_is_running('position_joint_trajectory_controller')
        if not running:
            self.change_controller()
        self._move_group.clear_pose_targets()

        if not self._motion_stopped:
            self._move_group.set_pose_target(target_pose)
            plan = self._move_group.plan(target_pose)
            if plan.joint_trajectory.points:
                if message:
                    rospy.loginfo(str(message))
                move_success = self._move_group.execute(plan, wait=True)
            else:
                rospy.loginfo("Trajectory planning has failed")
                move_success = False
        else:
            rospy.loginfo("Motion is stopped. Aborting movement")
            move_success=False

        self._move_group.stop()
        self._move_group.clear_pose_targets()

        if move_success:
            move_success = all_close(self._move_group.get_current_pose().pose, target_pose)

        return move_success

    def go_to_pose_callback(self, req):

        target_pose = self._move_group.get_current_pose()
        target_pose.pose.position = req.target_pose.pose.position
        target_pose.pose.orientation = req.target_pose.pose.orientation

        move_success = self.go_to_pose(target_pose, message='Executing motion')

        return move_success

    def go_home_callback(self, req):

        use_joint_values = req.use_joint_values

        return self.go_home(use_joints=use_joint_values, home_gripper=req.home_gripper, go_drop=req.go_drop)


    def gripper_command_callback(self, req, speed=None, target_force=None, wait=True, timeout=None, verbose=True):
        if timeout is None:
            timeout = rospy.Duration.from_sec(2.0)

        # Handle weird command
        if req.close_gripper and req.open_gripper:
            rospy.loginfo("Gripper command invalid. No operation will be performed")
            return False

        if req.close_gripper:
            if verbose:
                print('self.close_gripper')
            #return self.close_gripper(speed=speed, target_force=target_force)
            return self.close_gripper(speed=req.velocity, target_force=req.target_force, wait=wait, timeout=timeout)

        elif req.open_gripper:
            if verbose:
                print('self.open_gripper')

            #return self.open_gripper(speed=speed)
            return self.open_gripper(speed=req.velocity, wait=wait, timeout=timeout)

        else:
            if verbose:
                print('self.command_gripper')
            #return self.command_gripper(req.width, target_force=target_force, velocity=speed)
            return self.command_gripper(req.width, target_force=req.target_force, velocity=req.velocity, wait=wait, timeout=timeout)

    def execute_trajectory(self, waypoints, constraints=None, eef_step=0.01):
        running = self.controller_is_running('position_joint_trajectory_controller')
        if not running:
            self.change_controller()
        # Plan and execute trajectory based on an array of pose waypoints

        # Plan
        (plan, fraction) = self._move_group.compute_cartesian_path(
                                waypoints=waypoints,
                                eef_step=eef_step,
                                jump_threshold=0.0,
                                avoid_collisions=True,
                                path_constraints=constraints
                                )

        # Retime trajectory according to velocity/acceleration limits
        plan = self._move_group.retime_trajectory(self._robot.get_current_state(),
                                        plan,
                                        velocity_scaling_factor=self._max_velocity_scaling_factor,
                                        acceleration_scaling_factor=self._max_acceleration_scaling_factor)

        if fraction > 0.5:
            msg = "Moving robot arm. Planned " + str(fraction) + " of the trajectory"
            rospy.loginfo(msg)
            self._move_group.execute(plan, wait=True)
            self._move_group.stop()
            self._move_group.clear_pose_targets()
            res = all_close(self._move_group.get_current_pose().pose, waypoints[-1])
            return res
        else:
            msg = "Plan failed. Planned " + str(fraction*100) + "% of the trajectory"
            rospy.loginfo(msg)
            self._move_group.stop()
            self._move_group.clear_pose_targets()
            return False

    def execute_trajectory_callback(self, req):
        running = self.controller_is_running('position_joint_trajectory_controller')
        if not running:
            self.change_controller()

        if req.with_approach_constraint:
            # Try enforcing a constraint during approach
            approach_constraints = moveit_msgs.msg.Constraints()
            orient_constraint = moveit_msgs.msg.OrientationConstraint()
            orient_constraint.header.frame_id = "panda_link0"
            orient_constraint.link_name = self._eef_link
            orient_constraint.orientation = req.pose_waypoints.poses[-1].orientation
            orient_constraint.absolute_x_axis_tolerance = 0.1
            orient_constraint.absolute_y_axis_tolerance = 0.1
            orient_constraint.absolute_z_axis_tolerance = 0.1
            orient_constraint.weight = 1.0
            approach_constraints.orientation_constraints.append(orient_constraint)
        else:
            approach_constraints = None
        
        # Plan and execute trajectory based on an array of pose waypoints
        waypoints = []
        pose_start = self._move_group.get_current_pose().pose
        waypoints.append(pose_start)

        # Add all the waypoints from the message
        for wp in req.pose_waypoints.poses:
            waypoints.append(wp)

        # Plan
        (plan, fraction) = self._move_group.compute_cartesian_path(
                                waypoints=waypoints,
                                eef_step=0.01,
                                jump_threshold=0.0,
                                avoid_collisions=True,
                                path_constraints=approach_constraints
                                )

        # Retime trajectory according to velocity/acceleration limits
        plan = self._move_group.retime_trajectory(self._robot.get_current_state(),
                                        plan,
                                        velocity_scaling_factor=self._max_velocity_scaling_factor,
                                        acceleration_scaling_factor=self._max_acceleration_scaling_factor)

        if fraction > 0.5:
            msg = std_msgs.msg.String("Moving robot arm. Planned " + str(fraction) + " of the trajectory")
            rospy.loginfo(msg)
            self._move_group.execute(plan, wait=True)
            self._move_group.stop()
            self._move_group.clear_pose_targets()
            res = all_close(self._move_group.get_current_pose().pose, waypoints[-1])

            return PandaMoveWaypointsResponse(
                success=res,
                message=msg
            )
        else:
            msg = std_msgs.msg.String("Plan failed. Planned " + str(fraction*100) + "% of the trajectory")
            rospy.loginfo(msg)
            self._move_group.stop()
            self._move_group.clear_pose_targets()
            return PandaMoveWaypointsResponse(
                success=False,
                message=msg
            )

    def stop_motion_callback(self, req):

        self._move_group.stop()
        self._gripper.stop_gripper()
        self.set_stopped_status(stopped=True)

        return TriggerResponse(
                    success=True,
                    message="Motion stopped"
        )

    def recover_error_callback(self, req):

        # Recover from error and reset motion status

        from franka_msgs.msg import ErrorRecoveryActionGoal

        pub = rospy.Publisher("/franka_control/error_recovery/goal", ErrorRecoveryActionGoal, queue_size=10, latch=True)
        msg = ErrorRecoveryActionGoal()
        for i in range(10):
            pub.publish(msg)
            rospy.sleep(0.1)

        self.set_stopped_status(stopped=False)

        return TriggerResponse(
                    success=True,
                    message="Recovered from error"
        )

    def set_vel_accel_scaling_factors(self, vel_scal_fac, acc_scal_fac):

        # Set max velocity and acceleration scaling factors

        self._max_velocity_scaling_factor = vel_scal_fac
        self._max_acceleration_scaling_factor = acc_scal_fac

        self._move_group.set_max_velocity_scaling_factor(vel_scal_fac)
        self._move_group.set_max_acceleration_scaling_factor(acc_scal_fac)

    def set_vel_accel_scaling_factor_callback(self, req):

        self.set_vel_accel_scaling_factors(req.new_vel_scaling_factor,
                                           req.new_accel_scaling_factor)

        return True

    def grasp(self, width, velocity=0.5, force=20):

        # Execute grasp directly with the gripper action server
        # Different behaviour according to the enable_force_grasp flag
        if self._enable_force_grasp:

            grasp_result = self._gripper.grasp_motion(width, velocity, force)

        else:

            grasp_result = self._gripper.move_fingers(width, velocity, force)

        return grasp_result

    def check_trajectory_feasibility(self, goals):

        # Check if a trajectory from the current status to the end goal
        # can be computed successfully
        # Return true or false

        robot_initial_state = self._move_group.get_current_state()
        robot_current_state = copy.deepcopy(robot_initial_state)

        for next_goal in goals:

            # Plan from current state to next goal
            self._move_group.clear_pose_targets()
            self._move_group.set_start_state(robot_current_state)
            self._move_group.set_pose_target(next_goal)
            current_plan = self._move_group.plan()

            # Return if any part of the plan is not feasible
            if not current_plan.joint_trajectory.points:
                self._move_group.clear_pose_targets()
                self._move_group.set_start_state_to_current_state()
                return False

            # If a plan is feasible, set final goal of previous plan a current state
            joint_val_list = list(robot_current_state.joint_state.position)
            for joint_idx, joint_val in enumerate(current_plan.joint_trajectory.points[-1].positions):
                joint_val_list[joint_idx] = joint_val
            robot_current_state.joint_state.position = tuple(joint_val_list)

        # If we got this far, the whole thing is feasible
        self._move_group.clear_pose_targets()
        self._move_group.set_start_state_to_current_state()
        return True

    #todo get force grasp, velocity?
    def do_grasp_callback(self, req, verbose=True):

        rospy.loginfo('%s: Executing grasp' %
                      (self._grasp_service.resolved_name))

        # First of all, define a bunch of intermediate goals
        # Pregrasp
        # Lift pose
        # Drop pose

        target_grasp_pose_msg = copy.deepcopy(req.grasp.pose)
       

        # ---------------- Compute pregrasp pose
        # Transform grasp in homogeneous matrix notation
        target_grasp_pose_q = req.grasp.pose.orientation
        target_grasp_pose_p = req.grasp.pose.position
        target_grasp_pose = quaternion_matrix([target_grasp_pose_q.x,
                                              target_grasp_pose_q.y,
                                              target_grasp_pose_q.z,
                                              target_grasp_pose_q.w])
        target_grasp_pose[:3, 3] = np.array([target_grasp_pose_p.x,
                                             target_grasp_pose_p.y,
                                             target_grasp_pose_p.z])

        # Define a pre-grasp point along the approach axis
        approach_offset = quaternion_matrix([0., 0., 0., 1.])
        approach_offset[:3, 3] = np.array([0., 0., -0.15])

        # Create pregrasp pose
        pregrasp_pose = geometry_msgs.msg.Pose()

        pregrasp = np.matmul(target_grasp_pose, approach_offset)
        pregrasp_q = quaternion_from_matrix(pregrasp)

        pregrasp_pose.orientation.x = pregrasp_q[0]
        pregrasp_pose.orientation.y = pregrasp_q[1]
        pregrasp_pose.orientation.z = pregrasp_q[2]
        pregrasp_pose.orientation.w = pregrasp_q[3]

        pregrasp_pose.position.x = pregrasp[0, 3]
        pregrasp_pose.position.y = pregrasp[1, 3]
        pregrasp_pose.position.z = pregrasp[2, 3]


        #compute fliped pregrasp pose

        target_grasp_pose_flip = np.dot(target_grasp_pose,
                                    quaternion_matrix([0, 0, 1, 0]))

        # Create pregrasp pose
        pregrasp_pose_flip = geometry_msgs.msg.Pose()

        pregrasp_flip = np.matmul(target_grasp_pose_flip, approach_offset)
        pregrasp_q_flip = quaternion_from_matrix(pregrasp_flip)

        pregrasp_pose_flip.orientation.x = pregrasp_q_flip[0]
        pregrasp_pose_flip.orientation.y = pregrasp_q_flip[1]
        pregrasp_pose_flip.orientation.z = pregrasp_q_flip[2]
        pregrasp_pose_flip.orientation.w = pregrasp_q_flip[3]

        pregrasp_pose_flip.position.x = pregrasp_flip[0, 3]
        pregrasp_pose_flip.position.y = pregrasp_flip[1, 3]
        pregrasp_pose_flip.position.z = pregrasp_flip[2, 3]


        target_grasp_pose_flip_msg = copy.deepcopy(req.grasp.pose)
        target_grasp_pose_flip_msg.orientation = pregrasp_pose_flip.orientation

        # ---------------- Compute lift pose
        lift_pose = copy.deepcopy(pregrasp_pose)
        #lift_pose.position.z += 0.15
        lift_pose.position.z += 0.2

        # ---------------- Compute lift pose fliped
        lift_pose_flipped = copy.deepcopy(pregrasp_pose_flip)
        #lift_pose.position.z += 0.15
        lift_pose_flipped.position.z += 0.2

        # ---------------- Compute dropoff pose
        drop_pose = copy.deepcopy(req.grasp.pose)
        '''
        drop_pose.orientation.x = 1
        drop_pose.orientation.y = 0
        drop_pose.orientation.z = 0
        drop_pose.orientation.w = 0

        drop_pose.position.x = 0.45
        drop_pose.position.y = -0.5
        drop_pose.position.z = 0.4
        '''
        drop_pose.orientation.x = self._drop_pose.orientation.x
        drop_pose.orientation.y = self._drop_pose.orientation.y
        drop_pose.orientation.z = self._drop_pose.orientation.z
        drop_pose.orientation.w = self._drop_pose.orientation.w

        drop_pose.position.x = self._drop_pose.position.x
        drop_pose.position.y = self._drop_pose.position.y
        drop_pose.position.z = self._drop_pose.position.z

        # If just testing, return such result
        if req.plan_only:
            goals = [pregrasp_pose, lift_pose, drop_pose]
            return self.check_trajectory_feasibility(goals)

        #if not self.go_home(use_joints=True):
        #    return False

        # Move fingers in pre grasp pose
        # self.command_gripper(req.width.data)
        #self.open_gripper()
        grasp_success = self.command_gripper(req.width.data, target_force=None, velocity=None)
        if not grasp_success:
            print('-- self.command_gripper(req.width.data) Failure')
        else:
            print('-- self.command_gripper(req.width.data) Success')

        # We acquire the board pose, if needed, before moving the robot
        if self._save_grasp:
            graspa_board_pose = graspa_utils.get_GRASPA_board_pose(self._tf_listener)
        else:
            graspa_board_pose = None
    
        # Add a collision volume to the end effector, to make sure
        # whatever object is being carried does not bump into the table or anything else
        attached_object_pose = geometry_msgs.msg.PoseStamped()
        attached_object_pose.header.frame_id = self._eef_link
        attached_object_pose.pose.orientation.w = 1.0
        attached_object_pose.pose.position.z = 0.01
        if isinstance(self._gripper, grippers.FrankaHandGripper):
            self._scene.attach_box('panda_tcp', 'attached_object', pose=attached_object_pose, size=[0.05,0.05,0.05],
                                   touch_links=['panda_hand', 'panda_rightfinger', 'panda_leftfinger'])
        elif isinstance(self._gripper, (grippers.Robotiq2FGripper, grippers.Robotiq2FGripperForceControlled)):
            self._scene.attach_box('panda_tcp', 'attached_object', pose=attached_object_pose, size=[0.1,0.1,0.1],
                                   touch_links=['left_inner_finger', 'right_inner_finger',
                                                'left_inner_finger_pad', 'right_inner_finger_pad',
                                                'left_inner_knuckle', 'right_inner_knuckle',
                                                'left_outer_finger', 'right_outer_finger',
                                                'left_outer_knuckle', 'right_outer_knuckle',
                                                'robotiq_arg2f_base_link'])

        res = self.execute_trajectory([self._move_group.get_current_pose().pose, pregrasp_pose])
        if not res:                   
            res = self.go_to_pose(pregrasp_pose, "Moving to pregrasp pose")

         # Let go of the collision box
        self._scene.remove_attached_object('panda_tcp', 'attached_object')
        while len(self._scene.get_attached_objects(['attached_object']).keys()):
            rospy.sleep(0.1)
        self._scene.remove_world_object('attached_object')

        if not res:
            res = self.execute_trajectory([self._move_group.get_current_pose().pose, pregrasp_pose])
            if not res:
                res = self.go_to_pose(pregrasp_pose, "Moving to pregrasp pose")
                if not res:
                    return False
        if verbose:
            print('-- pregrasp_pose: ', all_close(self._move_group.get_current_pose().pose, pregrasp_pose))

        flipped = not res
        if flipped:
            # If there is no plan available for this grasp pose, try flipping it around the approach axis and replan
            #raw_input()

            rospy.logwarn("1 Target pose unreachable. Replanning with flipped pose.")

            # Add a collision volume to the end effector, to make sure
            # whatever object is being carried does not bump into the table or anything else
            attached_object_pose = geometry_msgs.msg.PoseStamped()
            attached_object_pose.header.frame_id = self._eef_link
            attached_object_pose.pose.orientation.w = 1.0
            attached_object_pose.pose.position.z = 0.01
            if isinstance(self._gripper, grippers.FrankaHandGripper):
                self._scene.attach_box('panda_tcp', 'attached_object', pose=attached_object_pose, size=[0.05,0.05,0.05],
                                    touch_links=['panda_hand', 'panda_rightfinger', 'panda_leftfinger'])
            elif isinstance(self._gripper, (grippers.Robotiq2FGripper, grippers.Robotiq2FGripperForceControlled)):
                self._scene.attach_box('panda_tcp', 'attached_object', pose=attached_object_pose, size=[0.1,0.1,0.1],
                                    touch_links=['left_inner_finger', 'right_inner_finger',
                                                    'left_inner_finger_pad', 'right_inner_finger_pad',
                                                    'left_inner_knuckle', 'right_inner_knuckle',
                                                    'left_outer_finger', 'right_outer_finger',
                                                    'left_outer_knuckle', 'right_outer_knuckle',
                                                    'robotiq_arg2f_base_link'])

            res = self.execute_trajectory([self._move_group.get_current_pose().pose, pregrasp_pose_flip])
            if not res:
                res = self.go_to_pose(pregrasp_pose_flip, "Moving to pregrasp_pose_flip ")

              # Let go of the collision box
            self._scene.remove_attached_object('panda_tcp', 'attached_object')
            while len(self._scene.get_attached_objects(['attached_object']).keys()):
                rospy.sleep(0.1)
            self._scene.remove_world_object('attached_object')

            res = self.execute_trajectory([self._move_group.get_current_pose().pose, pregrasp_pose_flip])
            if not res:
                res = self.go_to_pose(pregrasp_pose_flip, "Moving to pregrasp_pose_flip ")

            if verbose:
                print('-- pregrasp_pose_flip: ', all_close(self._move_group.get_current_pose().pose, pregrasp_pose_flip))

        if not res:
            self.go_home(use_joints=True)
            return False

        above_pose = self._move_group.get_current_pose().pose
        approach_waypoints = []
        n_approach_waypoints = 10
        approach_range_x = target_grasp_pose_p.x - above_pose.position.x
        approach_range_y = target_grasp_pose_p.y - above_pose.position.y
        approach_range_z = target_grasp_pose_p.z - above_pose.position.z
       

        if flipped:
            for idx_waypoint in range(n_approach_waypoints + 1):
                wp = copy.deepcopy(pregrasp_pose_flip)
                wp.position.x = above_pose.position.x + approach_range_x * idx_waypoint / n_approach_waypoints
                wp.position.y = above_pose.position.y + approach_range_y * idx_waypoint / n_approach_waypoints
                wp.position.z = above_pose.position.z + approach_range_z * idx_waypoint / n_approach_waypoints
                approach_waypoints.append(wp)

            # Try enforcing a constraint during approach
            approach_constraints = moveit_msgs.msg.Constraints()
            orient_constraint = moveit_msgs.msg.OrientationConstraint()
            orient_constraint.header.frame_id = "panda_link0"
            orient_constraint.link_name = self._eef_link
            orient_constraint.absolute_x_axis_tolerance = 0.1
            orient_constraint.absolute_y_axis_tolerance = 0.1
            orient_constraint.absolute_z_axis_tolerance = 0.1
            orient_constraint.weight = 1.0
            orient_constraint.orientation = pregrasp_pose_flip.orientation
        else:
            for idx_waypoint in range(n_approach_waypoints + 1):
                wp = copy.deepcopy(pregrasp_pose)
                wp.position.x = above_pose.position.x + approach_range_x * idx_waypoint / n_approach_waypoints
                wp.position.y = above_pose.position.y + approach_range_y * idx_waypoint / n_approach_waypoints
                wp.position.z = above_pose.position.z + approach_range_z * idx_waypoint / n_approach_waypoints
                approach_waypoints.append(wp)

            # Try enforcing a constraint during approach
            approach_constraints = moveit_msgs.msg.Constraints()
            orient_constraint = moveit_msgs.msg.OrientationConstraint()
            orient_constraint.header.frame_id = "panda_link0"
            orient_constraint.link_name = self._eef_link
            orient_constraint.absolute_x_axis_tolerance = 0.1
            orient_constraint.absolute_y_axis_tolerance = 0.1
            orient_constraint.absolute_z_axis_tolerance = 0.1
            orient_constraint.weight = 1.0
            orient_constraint.orientation = pregrasp_pose.orientation
        
        approach_constraints.orientation_constraints.append(orient_constraint)

        res = self.execute_trajectory(approach_waypoints, approach_constraints)

        if verbose:
            print('-- req.grasp.pose: ', all_close(self._move_group.get_current_pose().pose, req.grasp.pose))

        if not res:
            
            flipped = not flipped
            
             # If there is no plan available for this grasp pose, try flipping it around the approach axis and replan
            #raw_input()

            rospy.logwarn("2 Target pose unreachable. Replanning with flipped pose.")

            # Add a collision volume to the end effector, to make sure
            # whatever object is being carried does not bump into the table or anything else
            attached_object_pose = geometry_msgs.msg.PoseStamped()
            attached_object_pose.header.frame_id = self._eef_link
            attached_object_pose.pose.orientation.w = 1.0
            attached_object_pose.pose.position.z = 0.01
            if isinstance(self._gripper, grippers.FrankaHandGripper):
                self._scene.attach_box('panda_tcp', 'attached_object', pose=attached_object_pose, size=[0.05,0.05,0.05],
                                    touch_links=['panda_hand', 'panda_rightfinger', 'panda_leftfinger'])
            elif isinstance(self._gripper, (grippers.Robotiq2FGripper, grippers.Robotiq2FGripperForceControlled)):
                self._scene.attach_box('panda_tcp', 'attached_object', pose=attached_object_pose, size=[0.1,0.1,0.1],
                                    touch_links=['left_inner_finger', 'right_inner_finger',
                                                    'left_inner_finger_pad', 'right_inner_finger_pad',
                                                    'left_inner_knuckle', 'right_inner_knuckle',
                                                    'left_outer_finger', 'right_outer_finger',
                                                    'left_outer_knuckle', 'right_outer_knuckle',
                                                    'robotiq_arg2f_base_link'])

            res = self.execute_trajectory([self._move_group.get_current_pose().pose, pregrasp_pose_flip])
            if not res:
                res = self.go_to_pose(pregrasp_pose_flip, "Moving to pregrasp_pose_flip ")

              # Let go of the collision box
            self._scene.remove_attached_object('panda_tcp', 'attached_object')
            while len(self._scene.get_attached_objects(['attached_object']).keys()):
                rospy.sleep(0.1)
            self._scene.remove_world_object('attached_object')

            res = self.execute_trajectory([self._move_group.get_current_pose().pose, pregrasp_pose_flip])
            if not res:
                res = self.go_to_pose(pregrasp_pose_flip, "Moving to pregrasp_pose_flip ")

            if verbose:
                print('-- pregrasp_pose_flip: ', all_close(self._move_group.get_current_pose().pose, pregrasp_pose_flip))

            above_pose = self._move_group.get_current_pose().pose
            approach_waypoints = []
            n_approach_waypoints = 10
            approach_range_x = target_grasp_pose_p.x - above_pose.position.x
            approach_range_y = target_grasp_pose_p.y - above_pose.position.y
            approach_range_z = target_grasp_pose_p.z - above_pose.position.z
        

            if flipped:
                for idx_waypoint in range(n_approach_waypoints + 1):
                    wp = copy.deepcopy(pregrasp_pose_flip)
                    wp.position.x = above_pose.position.x + approach_range_x * idx_waypoint / n_approach_waypoints
                    wp.position.y = above_pose.position.y + approach_range_y * idx_waypoint / n_approach_waypoints
                    wp.position.z = above_pose.position.z + approach_range_z * idx_waypoint / n_approach_waypoints
                    approach_waypoints.append(wp)

                # Try enforcing a constraint during approach
                approach_constraints = moveit_msgs.msg.Constraints()
                orient_constraint = moveit_msgs.msg.OrientationConstraint()
                orient_constraint.header.frame_id = "panda_link0"
                orient_constraint.link_name = self._eef_link
                orient_constraint.absolute_x_axis_tolerance = 0.1
                orient_constraint.absolute_y_axis_tolerance = 0.1
                orient_constraint.absolute_z_axis_tolerance = 0.1
                orient_constraint.weight = 1.0
                orient_constraint.orientation = pregrasp_pose_flip.orientation
            else:
                for idx_waypoint in range(n_approach_waypoints + 1):
                    wp = copy.deepcopy(pregrasp_pose)
                    wp.position.x = above_pose.position.x + approach_range_x * idx_waypoint / n_approach_waypoints
                    wp.position.y = above_pose.position.y + approach_range_y * idx_waypoint / n_approach_waypoints
                    wp.position.z = above_pose.position.z + approach_range_z * idx_waypoint / n_approach_waypoints
                    approach_waypoints.append(wp)

            # Try enforcing a constraint during approach
            approach_constraints = moveit_msgs.msg.Constraints()
            orient_constraint = moveit_msgs.msg.OrientationConstraint()
            orient_constraint.header.frame_id = "panda_link0"
            orient_constraint.link_name = self._eef_link
            orient_constraint.absolute_x_axis_tolerance = 0.1
            orient_constraint.absolute_y_axis_tolerance = 0.1
            orient_constraint.absolute_z_axis_tolerance = 0.1
            orient_constraint.weight = 1.0
            orient_constraint.orientation = pregrasp_pose.orientation
            
            approach_constraints.orientation_constraints.append(orient_constraint)
            res = self.execute_trajectory(approach_waypoints, approach_constraints)

            if not res:
                res = self.go_to_pose(target_grasp_pose_flip_msg, "Moving to target_grasp_pose_flip")

            if verbose:
                print('-- req.grasp.pose: ', all_close(self._move_group.get_current_pose().pose, target_grasp_pose_flip_msg))



        # if verbose:
        #     print('-- target_grasp_pose: ', all_close(self._move_group.get_current_pose().pose, target_grasp_pose_msg))
        #     print('-- target_grasp_pose_p_msg: ', all_close(self._move_group.get_current_pose().pose, target_grasp_pose_p_msg))

        if self._stop_before_closing:
            return res

        # Try to grasp. In case of failure, go back to pregrasp pose and go home
        self.close_gripper(speed=None, target_force=None)
        
        #
        
        #grasp_success = self.grasp(req.width.data)
        '''
        if not grasp_success:
            rospy.logwarn("Grasp failed!")
            self.open_gripper()
            self.go_to_pose(pregrasp_pose)
            self.go_home(use_joints=True)
            return False
        '''
        # res = self.execute_trajectory([self._move_group.get_current_pose().pose, pregrasp_pose])
        # if not res:
        #     self.go_to_pose(pregrasp_pose, message="Moving back from grasping pose")

        # if verbose:
        #     print('-- pregrasp_pose: ', all_close(self._move_group.get_current_pose().pose, pregrasp_pose))

        current_pose = self._move_group.get_current_pose().pose
        lift_pose.orientation.x = current_pose.orientation.x
        lift_pose.orientation.y = current_pose.orientation.y
        lift_pose.orientation.z = current_pose.orientation.z
        lift_pose.orientation.w = current_pose.orientation.w

        drop_pose.orientation.x = current_pose.orientation.x
        drop_pose.orientation.y = current_pose.orientation.y
        drop_pose.orientation.z = current_pose.orientation.z
        drop_pose.orientation.w = current_pose.orientation.w

        res = self.execute_trajectory([self._move_group.get_current_pose().pose, lift_pose])
        if not res:
            self.go_to_pose(lift_pose, message="Lifting from grasping pose")

        if verbose:
            print('-- lift_pose: ', all_close(self._move_group.get_current_pose().pose, lift_pose))


        # Check if grasp was successful
        #TODO: use external measured forces to estimate whether the object was grasped or not
        # gripper_state = self.get_gripper_state()
        # grasp_success = False if sum(gripper_state) <= 0.01 else True
        # rospy.loginfo("Gripper state: " +  str(gripper_state[0] + gripper_state[1]))
        # rospy.loginfo("Grasp success? " + str(grasp_success))

        # Add a collision volume to the end effector, to make sure
        # whatever object is being carried does not bump into the table or anything else
        attached_object_pose = geometry_msgs.msg.PoseStamped()
        attached_object_pose.header.frame_id = self._eef_link
        attached_object_pose.pose.orientation.w = 1.0
        attached_object_pose.pose.position.z = 0.1
        if isinstance(self._gripper, grippers.FrankaHandGripper):
            self._scene.attach_box('panda_tcp', 'attached_object', pose=attached_object_pose, size=[0.2,0.2,0.2],
                                   touch_links=['panda_hand', 'panda_rightfinger', 'panda_leftfinger'])
        elif isinstance(self._gripper, (grippers.Robotiq2FGripper, grippers.Robotiq2FGripperForceControlled)):
            self._scene.attach_box('panda_tcp', 'attached_object', pose=attached_object_pose, size=[0.2,0.2,0.2],
                                   touch_links=['left_inner_finger', 'right_inner_finger',
                                                'left_inner_finger_pad', 'right_inner_finger_pad',
                                                'left_inner_knuckle', 'right_inner_knuckle',
                                                'left_outer_finger', 'right_outer_finger',
                                                'left_outer_knuckle', 'right_outer_knuckle',
                                                'robotiq_arg2f_base_link'])

        # Check stability
        if self._enable_graspa_stab_motion:
            rospy.loginfo("Measuring lift stability")
            rospy.sleep(5)
            rospy.loginfo("Measuring grasp stability")
            self.evaluate_stability(lift_pose, [0.3, -0.3, 0.5])

        # Move object out of workspace
        self.go_to_pose(drop_pose, message="Dropping object away from workspace")

        if verbose:
            print('-- drop_pose: ', all_close(self._move_group.get_current_pose().pose, drop_pose))


        # Let go of the collision box
        self._scene.remove_attached_object('panda_tcp', 'attached_object')
        while len(self._scene.get_attached_objects(['attached_object']).keys()):
            rospy.sleep(0.1)
        self._scene.remove_world_object('attached_object')

        self.open_gripper(speed=None)

        self.go_home(use_joints=True)

        if verbose:
            print('-- home_pose: ', all_close(self._move_group.get_current_pose().pose, self._home_pose))

        if self._save_grasp:
            if graspa_board_pose:
                # Save the grasp
                save = raw_input("Save grasp? [y/N]")
                if save.lower() == 'y':
                    graspa_utils.save_GRASPA_grasp(self._grasp_save_path, req.grasp, graspa_board_pose)
            else:
                rospy.logwarn("Grasp will not be saved since GRASPA board was not detected in the scene")

        return grasp_success

     #todo get force grasp, velocity?
    def test_grasp(self, release):
        try:
            self.save_image_srv()
            rospy.sleep(self.wait_time_save_image_srv)
        except Exception as e:
            print(e)
            
        #current pose
        current_pose = self._move_group.get_current_pose().pose

        # ---------------- Compute lift pose
        lift_pose = copy.deepcopy(current_pose)
        #lift_pose.position.z += 0.15
        #lift_pose.position.z += 0.2
        #if lift_pose.position.z > 0.5:
        # if lift_pose.position.z < 0.48:
        #     lift_pose.position.z = 0.48
        lift_pose.position.z = 0.45


        #if not self.go_to_pose(lift_pose, message="Lifting from grasping pose"):
            #return False
        r = self.execute_trajectory([self._move_group.get_current_pose().pose, lift_pose])

        # (plan, fraction) = self._move_group.compute_cartesian_path(
        #     [self._move_group.get_current_pose(), lift_pose], 0.01, 0.0  # waypoints to follow  # eef_step
        # )  # jump_threshold
        # r = self._move_group.execute(plan, wait=False)

        if not r:
            r = self.go_to_pose(lift_pose, message="Lifting from grasping pose")
            if not r:
                return False
        # ---------------- Compute dropoff pose
        drop_pose = copy.deepcopy(current_pose)
        '''
        drop_pose.orientation.x = 1
        drop_pose.orientation.y = 0
        drop_pose.orientation.z = 0
        drop_pose.orientation.w = 0

        drop_pose.position.x = 0.45
        drop_pose.position.y = -0.5
        drop_pose.position.z = 0.4
        '''
        # drop_pose.orientation.x = self._drop_pose.orientation.x
        # drop_pose.orientation.y = self._drop_pose.orientation.y
        # drop_pose.orientation.z = self._drop_pose.orientation.z
        # drop_pose.orientation.w = self._drop_pose.orientation.w

        # drop_pose.position.x = self._drop_pose.position.x
        # drop_pose.position.y = self._drop_pose.position.y
        # drop_pose.position.z = self._drop_pose.position.z


        # Try to grasp. In case of failure, go back to pregrasp pose and go home
        #self.close_gripper(speed=None, target_force=None)
        


        # Check if grasp was successful
        #TODO: use external measured forces to estimate whether the object was grasped or not
        # gripper_state = self.get_gripper_state()
        # grasp_success = False if sum(gripper_state) <= 0.01 else True
        # rospy.loginfo("Gripper state: " +  str(gripper_state[0] + gripper_state[1]))
        # rospy.loginfo("Grasp success? " + str(grasp_success))

        # Add a collision volume to the end effector, to make sure
        # whatever object is being carried does not bump into the table or anything else
        attached_object_pose = geometry_msgs.msg.PoseStamped()
        attached_object_pose.header.frame_id = self._eef_link
        attached_object_pose.pose.orientation.w = 1.0
        attached_object_pose.pose.position.z = 0.1
        if isinstance(self._gripper, grippers.FrankaHandGripper):
            self._scene.attach_box('panda_tcp', 'attached_object', pose=attached_object_pose, size=[0.2,0.2,0.2],
                                   touch_links=['panda_hand', 'panda_rightfinger', 'panda_leftfinger'])
        elif isinstance(self._gripper, (grippers.Robotiq2FGripper, grippers.Robotiq2FGripperForceControlled)):
            self._scene.attach_box('panda_tcp', 'attached_object', pose=attached_object_pose, size=[0.2,0.2,0.2],
                                   touch_links=['left_inner_finger', 'right_inner_finger',
                                                'left_inner_finger_pad', 'right_inner_finger_pad',
                                                'left_inner_knuckle', 'right_inner_knuckle',
                                                'left_outer_finger', 'right_outer_finger',
                                                'left_outer_knuckle', 'right_outer_knuckle',
                                                'robotiq_arg2f_base_link'])


        # Check stability
        rospy.loginfo("Measuring grasp stability")
        #self.evaluate_stability(lift_pose, [0.3, -0.3, 0.5])
        self.evaluate_stability(lift_pose, [lift_pose.position.x, lift_pose.position.y, lift_pose.position.z])

        
        r = self.execute_trajectory([self._move_group.get_current_pose().pose, drop_pose])

        # Move object out of workspace
        # (plan, fraction) = self._move_group.compute_cartesian_path(
        #     [self._move_group.get_current_pose(), drop_pose], 0.01, 0.0  # waypoints to follow  # eef_step
        # )  # jump_threshold
        # r = self._move_group.execute(plan, wait=False)

        if not r:
            r = self.go_to_pose(drop_pose, message="Dropping object away from workspace")
        if r:
            try:
                self.save_image_srv()
                rospy.sleep(self.wait_time_save_image_srv)
            except Exception as e:
                print(e)
            if release:
                self.open_gripper(speed=None)

        # Let go of the collision box
        self._scene.remove_attached_object('panda_tcp', 'attached_object')
        while len(self._scene.get_attached_objects(['attached_object']).keys()):
            rospy.sleep(0.1)
        self._scene.remove_world_object('attached_object')

        if not r:
            r = self.execute_trajectory([self._move_group.get_current_pose().pose, drop_pose])
            if not r:
                self.go_to_pose(drop_pose, message="Dropping object away from workspace")
            #todo save digit
            try:
                self.save_image_srv()
                rospy.sleep(self.wait_time_save_image_srv)
            except Exception as e:
                print(e)
            if release:
                self.open_gripper(speed=None)

        return True
    # def evaluate_stability(self, grasp_pose, tcp_travel, approach_rotation_angle=np.pi/4, binormal_rotation_angle=np.pi/3):

    #     # Evaluate stability of the grasp according to GRASPA v1.0
    #     # Approach and binormal rotation angles are in radians
    #     # tcp_travel is the 3D point to take the tcp to (while maintaining orientation) before executing rotations

    #     # Get initial orientation as a 4x4 homogeneous matrix
    #     # Rotation is the grasp rotation
    #     # Position of the tcp is tcp_travel
    #     grasp_orientation_quat = [grasp_pose.orientation.x,
    #                               grasp_pose.orientation.y,
    #                               grasp_pose.orientation.z,
    #                               grasp_pose.orientation.w]
    #     grasp_orientation_m = quaternion_matrix(grasp_orientation_quat)

    #     evaluation_center_pose = grasp_orientation_m
    #     evaluation_center_pose[:3, 3] = np.array([tcp_travel[0],
    #                                               tcp_travel[1],
    #                                               tcp_travel[2]])

    #     # Compute rotations around axes in matrix form
    #     approach_axis = grasp_orientation_m[:3, 2]
    #     binormal_axis = grasp_orientation_m[:3, 1]

    #     binormal_appr_rots = []
    #     appr_rots = []
    #     for i in range(4):
    #         binormal_appr_rots.append(rotation_matrix(angle=binormal_rotation_angle,
    #                                          direction=binormal_axis,
    #                                          point=np.zeros(3)
    #                                          ))
    #         appr_rots.append(rotation_matrix(angle=i*approach_rotation_angle,
    #                                          direction=approach_axis,
    #                                          point=np.zeros(3)
    #                                          ))
    #     # appr_rot_positive = rotation_matrix(angle=approach_rotation_angle,
    #     #                                     direction=approach_axis,
    #     #                                     point=np.zeros(3)
    #     #                                     )
    #     # appr_rot_negative = rotation_matrix(angle=-approach_rotation_angle,
    #     #                                     direction=approach_axis,
    #     #                                     point=np.zeros(3)
    #     #                                     )
    #     # bin_rot_positive = rotation_matrix(angle=binormal_rotation_angle,
    #     #                                    direction=binormal_axis,
    #     #                                    point=np.zeros(3)
    #     #                                    )
    #     # bin_rot_negative = rotation_matrix(angle=-binormal_rotation_angle,
    #     #                                    direction=binormal_axis,
    #     #                                    point=np.zeros(3)
    #     #                                    )

    #     appr_rot_poses = []
    #     cu_evaluation_center_pose = evaluation_center_pose
    #     for i in range(len(appr_rots)):
    #         appr_rot_poses.append(np.dot(np.dot(evaluation_center_pose, appr_rots[i]), binormal_appr_rots[i]))


    #     # Obtain target poses
    #     # appr_rot_positive_pose = np.dot(evaluation_center_pose, appr_rot_positive)
    #     # appr_rot_negative_pose = np.dot(evaluation_center_pose, appr_rot_negative)
    #     # bin_rot_positive_pose = np.dot(evaluation_center_pose, bin_rot_positive)
    #     # bin_rot_negative_pose = np.dot(evaluation_center_pose, bin_rot_negative)
    #     # bin_rot_positive_pose2 = np.dot(appr_rot_positive_pose, bin_rot_positive)
    #     # bin_rot_negative_pose2 = np.dot(appr_rot_negative_pose, bin_rot_negative)

    #     def numpy_to_pose(pose_4x4, tweaked=False):

    #         pose_msg = geometry_msgs.msg.Pose()

    #         # order as in geometry_msg/Pose.msg
    #         position_list = pose_4x4[:3, 3].tolist()
    #         orientation_list = quaternion_from_matrix(pose_4x4).tolist()

    #         pose_msg.position.x, pose_msg.position.y, pose_msg.position.z = [val for val in position_list]
    #         pose_msg.orientation.x, pose_msg.orientation.y, pose_msg.orientation.z, pose_msg.orientation.w = [val for val in orientation_list]

    #         if tweaked:
    #             pose_msg.orientation.x += np.pi/4

    #         return pose_msg

    #     # wp_1 = numpy_to_pose(appr_rot_positive_pose)
    #     # wp_2 = numpy_to_pose(appr_rot_negative_pose)
    #     # wp_3 = numpy_to_pose(bin_rot_positive_pose)
    #     # wp_4 = numpy_to_pose(bin_rot_negative_pose)
    #     # wp_5 = numpy_to_pose(bin_rot_positive_pose2)
    #     # wp_6 = numpy_to_pose(bin_rot_negative_pose2)

    #     wp_start = numpy_to_pose(evaluation_center_pose)
        
    #     # Create waypoint list (strictly GRASPA waypoints)
    #     # waypoints = [wp_start,
    #     #              wp_3,
    #     #              wp_5,
    #     #              wp_6,
    #     #              wp_4,
    #     #              wp_start]
        
    #     waypoints = [wp_start]
    #     i = -1
    #     for a in appr_rot_poses:
    #         i+=1
    #         waypoints.append(numpy_to_pose(appr_rot_poses[i]))
    #         waypoints.append(numpy_to_pose(appr_rot_poses[i], tweaked=True))

    #     waypoints.append(wp_start)    

    #     # Add another stability movement for the sake of completion (not GRASPA compliant)
    #     # waypoints.append(wp_4)
    #     # waypoints.append(wp_start)

    #     # Move the robot in a continuous trajectory
    #     # motion_success = self.execute_trajectory(waypoints)

    #     # Plan and perform each stability motion separately
    #     for wp in waypoints:
    #         self.go_to_pose(wp)

    #     return True
    def evaluate_stability(self, grasp_pose, tcp_travel, approach_rotation_angle=np.pi/2, binormal_rotation_angle=np.pi/4):
        print('evaluate_stability')
        # Evaluate stability of the grasp according to GRASPA v1.0
        # Approach and binormal rotation angles are in radians
        # tcp_travel is the 3D point to take the tcp to (while maintaining orientation) before executing rotations

        # Get initial orientation as a 4x4 homogeneous matrix
        # Rotation is the grasp rotation
        # Position of the tcp is tcp_travel
        grasp_orientation_quat = [grasp_pose.orientation.x,
                                  grasp_pose.orientation.y,
                                  grasp_pose.orientation.z,
                                  grasp_pose.orientation.w]
        grasp_orientation_m = quaternion_matrix(grasp_orientation_quat)

        evaluation_center_pose = grasp_orientation_m
        evaluation_center_pose[:3, 3] = np.array([tcp_travel[0],
                                                  tcp_travel[1],
                                                  tcp_travel[2]])

        # Compute rotations around axes in matrix form
        approach_axis = grasp_orientation_m[:3, 2]
        appr_rot_positive = rotation_matrix(angle=approach_rotation_angle,
                                            direction=approach_axis,
                                            point=np.zeros(3)
                                            )
        appr_rot_negative = rotation_matrix(angle=-approach_rotation_angle,
                                            direction=approach_axis,
                                            point=np.zeros(3)
                                            )
        binormal_axis = grasp_orientation_m[:3, 1]
        bin_rot_positive = rotation_matrix(angle=binormal_rotation_angle,
                                           direction=binormal_axis,
                                           point=np.zeros(3)
                                           )
        bin_rot_negative = rotation_matrix(angle=-binormal_rotation_angle,
                                           direction=binormal_axis,
                                           point=np.zeros(3)
                                           )


        # Obtain target poses
        appr_rot_positive_pose = np.dot(evaluation_center_pose, appr_rot_positive)
        appr_rot_negative_pose = np.dot(evaluation_center_pose, appr_rot_negative)
        bin_rot_positive_pose = np.dot(evaluation_center_pose, bin_rot_positive)
        bin_rot_negative_pose = np.dot(evaluation_center_pose, bin_rot_negative)

        def numpy_to_pose(pose_4x4):

            pose_msg = geometry_msgs.msg.Pose()

            # order as in geometry_msg/Pose.msg
            position_list = pose_4x4[:3, 3].tolist()
            orientation_list = quaternion_from_matrix(pose_4x4).tolist()

            pose_msg.position.x, pose_msg.position.y, pose_msg.position.z = [val for val in position_list]
            pose_msg.orientation.x, pose_msg.orientation.y, pose_msg.orientation.z, pose_msg.orientation.w = [val for val in orientation_list]

            return pose_msg

        wp_1 = numpy_to_pose(appr_rot_positive_pose)
        wp_2 = numpy_to_pose(appr_rot_negative_pose)
        wp_3 = numpy_to_pose(bin_rot_positive_pose)
        wp_4 = numpy_to_pose(bin_rot_negative_pose)
        wp_start = numpy_to_pose(evaluation_center_pose)

        # Create waypoint list (strictly GRASPA waypoints)
        waypoints = [wp_start,
                wp_1,
                wp_2,
                wp_3,
                wp_start,
                wp_4,
                wp_start]

        # waypoints = [wp_start,
        #              wp_1,
        #              wp_2,
        #              wp_3,
        #              wp_4,
        #              wp_start]

        # Add another stability movement for the sake of completion (not GRASPA compliant)
        # waypoints.append(wp_4)
        # waypoints.append(wp_start)

        # Move the robot in a continuous trajectory
        # motion_success = self.execute_trajectory(waypoints)

        # Plan and perform each stability motion separately
        i = -1
        for wp in waypoints:
            i+=1
            #self.go_to_pose(wp)
            r = self.execute_trajectory([self._move_group.get_current_pose().pose, wp])
            # (plan, fraction) = self._move_group.compute_cartesian_path(
            #     [self._move_group.get_current_pose(), wp], 0.01, 0.0  # waypoints to follow  # eef_step
            # )  # jump_threshold
            # r = self._move_group.execute(plan, wait=False)
            if not r:
                self.go_to_pose(wp)
            print("stability motion: ", i)
            try:
                self.save_image_srv()
                rospy.sleep(self.wait_time_save_image_srv)
            except Exception as e:
                print(e)
        return True
    

    #go to refined pose with trajectory constraints
    def attempt_grasp_refinement(self, grasp_offset=0.0, nb_of_intermediate_wp = 10, scale=1, verbose=False):
        #get current robot state

        # panda_state_req = PandaGetStateRequest()
        # panda_state_reply = self.panda_get_state_srv(panda_state_req)
        # print('panda_state_reply: ', panda_state_req)

        # current_pose_orientation = panda_state_reply.eef_state.pose.orientation
        # current_pose_position = panda_state_reply.eef_state.pose.position

        # print('current_pose_orientation: ', current_pose_orientation)
        # print('current_pose_position: ', current_pose_position)

        '''
        float64[7] joints_state
        float64 gripper_state
        geometry_msgs/PoseStamped eef_state
        '''

        # EEF pose
        eef_state = self._move_group.get_current_pose()

        # Joints state
        joints_state = self._move_group.get_current_joint_values()

        # Gripper width
        gripper_state = self._move_group_hand.get_current_joint_values()[0] + self._move_group_hand.get_current_joint_values()[1]

    
        # #build waypoints
        waypoints = []
        # '''
        # # request
        # geometry_msgs/PoseArray pose_waypoints # Ordered sequence of poses to traverse in trajectory
        # ---
        # # response
        # bool success
        # std_msgs/String message
        # '''
        #
        #
        wpose = eef_state.pose
        #wpose.position.z -= scale * 0.1  # First move up (z)
        #wpose.position.y += scale * 0.2  # and sideways (y)
        #waypoints.append(copy.deepcopy(wpose))

        factor_z = [-1.0, 1.0]
        factor_x = [-1.0, 1.0, -1.0, 1.0]
        factor_y = [1.0, -1.0, -1.0, 1.0]

        tweak_z = [factor_z[i%2] for i in range(0,nb_of_intermediate_wp)]
        tweak_x = [factor_x[i%4] for i in range(0,nb_of_intermediate_wp)]
        tweak_y = [factor_y[i%4] for i in range(0,nb_of_intermediate_wp)]

        for i in range(0,nb_of_intermediate_wp):
            wpose.position.z += tweak_z[i]*scale * 0.1  # First move up (z)
            wpose.position.y += tweak_y[i]*scale * 0.1  # and sideways (y)
            wpose.position.x += tweak_x[i]*scale * 0.1  # Second move forward/backwards in (x)
            
            c = i%4
            if c ==0:
                wpose.orientation.x = 0.992918373893
                wpose.orientation.y = 0.117632485859
                wpose.orientation.z = -0.0165593955607
                wpose.orientation.w = 0.0012196209585
            elif c==1:
                wpose.orientation.x = -0.706974796388
                wpose.orientation.y = 0.707131384133
                wpose.orientation.z = 0.0110741875876
                wpose.orientation.w = 0.0054041849752
            elif c ==2:
                wpose.orientation.x = -0.935205424766
                wpose.orientation.y = -0.0951732747089
                wpose.orientation.z = -0.336144709091
                wpose.orientation.w = 0.0577892361873
            elif c==3:
                wpose.orientation.x = 0.943112359767
                wpose.orientation.y = 0.000840354157913
                wpose.orientation.z = -0.332472942981
                wpose.orientation.w = 0.000335923620509


            waypoints.append(copy.deepcopy(wpose))

        #
        #wpose.position.x += scale * 0.1  # Second move forward/backwards in (x)
        #waypoints.append(copy.deepcopy(wpose))
        #
        #wpose.position.y -= scale * 0.1  # Third move sideways (y)
        #waypoints.append(copy.deepcopy(wpose))

        (plan, fraction) = self._move_group.compute_cartesian_path(
            waypoints, 0.01, 0.0  # waypoints to follow  # eef_step
        )  # jump_threshold

        self._move_group.execute(plan, wait=False)
        
        #self.execute_trajectory(waypoints)
        #
        # #get current orientation as dcm
        # rot = R.from_euler('xyz', euler)
        # cam_R_grasp = rot.as_dcm()
        #
        # cam_T_grasp = np.append(cam_R_grasp, np.array([grasp_pos]).T, axis=1)
        # cam_T_grasp = np.append(cam_T_grasp, np.array([[0, 0, 0, 1]]), axis=0)
        #
        # grasp_target_T_panda_ef = np.eye(4)
        # # grasp_target_T_panda_ef[2, 3] = -0.13
        # grasp_target_T_panda_ef[:3, 3] = grasp_offset
        #
        # cam_T_grasp = np.matmul(cam_T_grasp, grasp_target_T_panda_ef)
        #
        # pos = pose_6d[:3, 3]
        # rot = pose_6d[:3, :3]
        #
        # '''
        # position : (`numpy.ndarray` of float): 3-entry position vector wrt camera frame
        # rotation (`numpy.ndarray` of float):3x3 rotation matrix wrt camera frame
        # width : Distance between the fingers in meters.
        # score: prediction score of the grasp pose
        # ref_frame: frame of reference for camera that the grasp corresponds to.
        # quaternion: rotation expressed as quaternion
        # '''
        #
        # grasp_candidate = Grasp6D(position=pos, rotation=rot,
        #                           width=length_meters, score=float(quality),
        #                           ref_frame=intrinsic_params['frame'])
        #
        # if verbose:
        #     print('panda_control.execute_grasp:')
        #     print('x: ', x)
        #     print('y: ', y)
        #     print('z: ', z)
        #     print('euler: ', euler)
        #     print('length_meters: ', length_meters)
        #     print('candidate_offset: ', candidate_offset)
        #     print('quality: ', quality)
        #     print('pos: ', pos)
        #     print('rot: ', rot)
        #     print('grasp_candidate 6D: ', grasp_candidate)
        #
        # grasp_msg = BenchmarkGrasp()
        # p = PoseStamped()
        # p.header.frame_id = grasp_candidate.ref_frame
        # p.header.stamp = rospy.Time.now()
        # p.pose.position.x = grasp_candidate.position[0]
        # p.pose.position.y = grasp_candidate.position[1]
        # p.pose.position.z = grasp_candidate.position[2]
        # p.pose.orientation.w = grasp_candidate.quaternion[3]
        # p.pose.orientation.x = grasp_candidate.quaternion[0]
        # p.pose.orientation.y = grasp_candidate.quaternion[1]
        # p.pose.orientation.z = grasp_candidate.quaternion[2]
        #
        # camera_viewpoint = PoseStamped()
        # camera_viewpoint.header = camera_pose.header
        # camera_viewpoint.pose.orientation = camera_pose.transform.rotation
        # camera_viewpoint.pose.position = camera_pose.transform.translation
        # camera_viewpoint.header.stamp = rospy.Time.now()
        #
        # grasp_msg.pose = transform_grasp_to_world(p, camera_viewpoint)
        #
        # # Set candidate score and width
        # grasp_msg.score.data = grasp_candidate.score
        # grasp_msg.width.data = grasp_candidate.width
        # grasp_msg.offset = candidate_offset
        #
        # panda_grasp_req = PandaGraspRequest()
        # panda_grasp_req.grasp = grasp_msg.pose
        # if minimum_grasp_depth is not None:
        #     if panda_grasp_req.grasp.pose.position.z < minimum_grasp_depth:
        #         panda_grasp_req.grasp.pose.position.z = minimum_grasp_depth
        #
        # panda_grasp_req.width = grasp_msg.width
        #
        # if verbose:
        #     print("request to panda is: \n{}".format(panda_grasp_req))
        #
        # try:
        #     reply = self.panda_grasp_srv(panda_grasp_req)
        #     print("Service {} reply is: \n{}".format(self.panda_grasp_srv.resolved_name, reply))
        #     return True
        #
        # except rospy.ServiceException as e:
        #     print("Service {} call failed: {}".format(self.panda_grasp_srv.resolved_name, e))
        #     return False
    
def main():

    # Initialize the ROS node.
    rospy.init_node("panda_grasp_server")

    # Config
    config = NodeConfig()

    # Instantiate the action server.
    grasp_planner = PandaActionServer(config)

    # Spin forever.
    rospy.spin()
